let fire = document.getElementById('switch');
let tree = document.getElementById('christmas-tree');
let floor = document.getElementsByClassName('floor');
let darkLamp = tree.getElementsByClassName('cls-14');
let lightLamp = tree.getElementsByClassName('cls-15');
let tape = tree.getElementsByClassName('cls-21');
let star = tree.getElementById('Star');
let fadeit = tree.getElementsByClassName('fade');


const shine = () => {
    if (fire.checked) {
        document.body.style.backgroundColor="#281e1d";
        floor[0].style.backgroundColor="#725025";
        lightUp(fadeit);
        lightUp(darkLamp);
        lightUp(lightLamp);
        lightUp(tape);
        star.classList.add('fire');
    } else  {
        lightDown(darkLamp);
        lightDown(lightLamp);
        lightDown(tape);
        star.classList.remove('fire');
        document.body.style.backgroundColor="#6E5351";
        floor[0].style.backgroundColor="#996a30";
        lightDown(fadeit);
    }
};

const lightUp = (collection) => {
    for (let i = 0; i < collection.length; i++) {
        collection[i].classList.add('fire');
    }
};

const lightDown = (collection) => {
    for (let i = 0; i < collection.length; i++) {
        collection[i].classList.remove('fire');
    }
};
